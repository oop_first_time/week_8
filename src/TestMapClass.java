public class TestMapClass {
    private int width;
    private int height;
    private String name;

    public TestMapClass(String name, int width, int height) {
        this.name = name;
        this.width = width;
        this.height = height;
        
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName(String name) {
        return name;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    
    public void forloop() {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                System.out.print("-");
            }
            System.out.println();
        }
        
    }

    public void print() {
        System.out.println(name);
    }
}
