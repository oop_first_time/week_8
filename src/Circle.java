public class Circle {
    private String name;
    private double radian;

    public Circle(String name, double radian) {
        this.name = name;
        this.radian = radian;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName(String name) {
        return name;
    }

    public double areaCircle() {
        double areaC = Math.PI * (radian*radian);
        return areaC;
    }

    public double percircle(){
        double pc = (2*Math.PI*radian);
        return pc;
    }

    public void print() {
        System.out.println(name+"Radian = "+areaCircle());
        System.out.println(name+"perimeter = "+percircle());
    }
}
