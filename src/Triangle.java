public class Triangle {
    private String name;
    private double a;
    private double b;
    private double c;
    
    
    public Triangle(String name, double a, double b, double c) {
        this.name = name;
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName(String name) {
        return name;
    }

    public double getA(double a) {
        return a;
    }

    public double getB(double b) {
        return b;
    }

    public double getC(double c) {
        return c;
    }

    public double areaTriangle() {
        double sum = ((a+b+c)/2);
        double result = (sum*(sum-a)*(sum-b)*(sum-c));
        double areaTrai = Math.sqrt(result);
        return areaTrai;
    }

    public double perTriangle() {
        double perTrian = a+b+c;
        return perTrian;
    }


    public void print() {
        System.out.println(name+" calTriangle = "+areaTriangle());
        System.out.println(name+" perimeter = "+perTriangle());
    }


}
