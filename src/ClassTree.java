public class ClassTree {
    private String name;
    private int x;
    private int y;

    public ClassTree(String name, int x, int y) {
        this.name = name;
        this.x = x;
        this.y = y;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName(String name) {
        return name;
    }

    
    public int getX(int x) {
        return x;
    }

    public int getY(int y) {
        return y;
    }

    public void print() {
        System.out.println(name+" "+ "X:"+x+" "+ "Y:"+y);
    }
}
