public class TestShape {
    public static void main(String[] args) {
        //Circle
        Circle circle1 = new Circle("Circle1", 1);
        circle1.print();
        System.out.println();

        Circle circle2 = new Circle("Circle2", 2);
        circle2.print();
        System.out.println("-----------------------------");

        //Rectangle
        Rectangle rect1 = new Rectangle(10, 5, "rect1");
        rect1.print();
        System.out.println();

        Rectangle rect2 = new Rectangle(10, 5, "rect2");
        rect2.print();
        System.out.println("-----------------------------");

        //Triangle
        Triangle triangle1 = new Triangle("triangle1", 5, 5, 6);
        triangle1.print();
    }

}
