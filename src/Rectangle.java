public class Rectangle {
    private int width;
    private int height;
    private String name;

    public Rectangle(int width, int height, String name) {
        this.width = width;
        this.height = height;
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWidth(int width) {
        return width;
    }

    public int getHeight(int height) {
        return height;
    }

    public int calRectangle() {
        int cr = width *height;
        return cr;
    }

    public int perRectangle() {
        int perRec = (2 * (width +height));
        return perRec;
    }

    public void print() {
        System.out.println(name+" calRactanger= "+calRectangle());
        System.out.println(name+" perimeter= "+perRectangle());
    }

   



}
